#!/usr/bin/env python

from Tkinter import *
from datetime import datetime
import hardware

class ButtonHandler(hardware.ButtonHandler):
  def __init__(self, application):
    self.application = application
    self.light = "off"
    self.music = "off"

  def button1_pressed(self):
    self.application.status["text"] = "Button 1 pressed"
    self.application.updateDisplay()

  def button2_pressed(self):
    hardware.backlight.increase()
    self.application.status["text"] = "Increased backlight to %d" % hardware.backlight.get_level()
    self.application.updateDisplay()

  def button3_pressed(self):
    hardware.backlight.decrease()
    self.application.status["text"] = "Decreased backlight to %d" % hardware.backlight.get_level()
    self.application.updateDisplay()

  def button4_pressed(self):
    if self.music == "off":
      hardware.sound.play()
      self.music = "on"
    else:
      hardware.sound.stop()
      self.music = "off"
    self.application.status["text"] = "Music is now %s" % self.music
    self.application.updateDisplay()

  def snooze_pressed(self):
    if self.light == "off":
      hardware.light.set_color(hardware.light.COLOR_WHITE)
      self.light = "on"
    else:
      hardware.light.set_color(hardware.light.COLOR_BLACK)
      self.light = "off"
    self.application.status["text"] = "Turned light %s" % self.light
    self.application.updateDisplay()

class Application(Frame):

  def tick(self):
    self.updateDisplay()
    self.after(1000,self.tick)

  def updateDisplay(self):
    self.time["text"] = datetime.now().strftime('%H:%M')
    self.dow["text"] = datetime.now().strftime('%a')
    self.dom["text"] = datetime.now().strftime('%d')
    self.month["text"] = datetime.now().strftime('%b')

  def createWidgets(self):
    self["bg"] = "black"
  
    self.time = Label(self)
    self.time["text"] = "00:00"
    self.time["fg"] = "red"
    self.time["bg"] = self["bg"]
    self.time["font"] = ("Helvetica", 70)
    self.time.grid(row=1, column=2)

    self.date = Frame(self)
    self.date["bg"] = self["bg"]
    self.date.grid(row=1, column=1)
    
    self.dow = Label(self.date)
    self.dow["text"] = "sun"
    self.dow["bg"] = self["bg"]
    self.dow["fg"] = self.time["fg"]
    self.dow["font"] = ("Helvetica", 14)
    self.dow.grid(row=1, column=1)
  
    self.dom = Label(self.date)
    self.dom["text"] = "01"
    self.dom["bg"] = self["bg"]
    self.dom["fg"] = self.time["fg"]
    self.dom["font"] = ("Helvetica", 14)
    self.dom.grid(row=2, column=1)

    self.month = Label(self.date)
    self.month["text"] = "jan"
    self.month["bg"] = self["bg"]
    self.month["fg"] = self.time["fg"]
    self.month["font"] = ("Helvetica", 14)
    self.month.grid(row=3, column=1)
        
    self.status = Label(self)
    self.status["text"] = "Initialized"
    self.status["fg"] = "red"
    self.status["bg"] = self["bg"]
    self.status["font"] = ("Helvetica", 14)
    self.status.grid(column=2)

    hardware.buttons.set_handler(ButtonHandler(self))

	
  def __init__(self, master=None):
    Frame.__init__(self, master)
    master.attributes("-fullscreen",True)
    master.config(cursor='none')
    self.pack(fill=BOTH, expand=1)
    self.createWidgets()
    self.tick()

if __name__ == "__main__":
  root = Tk()
  app = Application(master=root)
  app.mainloop()
  root.destroy()
