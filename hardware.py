# Module for the hardware specific configuration of the alarm clock

import RPi.GPIO as GPIO
import neopixel
import vlc

class Backlight:
  """Handles the backlight of the screen."""
  
  BACKLIGHT = 18

  def __init__(self):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(self.BACKLIGHT,GPIO.OUT)
    self.pwm = GPIO.PWM(self.BACKLIGHT, 100)
    self.level = 50
    self.pwm.start(self.level)

  def __del__(self):
    self.pwm.stop()

  def get_level(self):
    """Returns the current backlight level (between 0 and 100)"""
    return self.level

  def set_level(self, level):
    """Sets the backlight level to a value between 0 and 100"""
    self.level = min(max(0,level),100)
    self.pwm.ChangeDutyCycle(self.level)
  
  def increase(self):
    """Increases the backlighting level by 5."""
    self.set_level(self.get_level()+5)
  
  def decrease(self):
    """Decreases the backlighting level by 5."""
    self.set_level(self.get_level()-5)

class ButtonHandler:
  """Abstract class that can be implemented and set as handler for button presses in the buttons class."""
  def button1_pressed(self):
    pass

  def button2_pressed(self):
    pass

  def button3_pressed(self):
    pass

  def button4_pressed(self):
    pass

  def snooze_pressed(self):
    pass


class Buttons:
  """This class configures the buttons and reacts to button presses.
     The button presses are forwarded to a ButtonHandler class."""

  BUTTON1 = 17
  BUTTON2 = 22
  BUTTON3 = 23
  BUTTON4 = 27
  SNOOZE  = 20

  def __init__(self):
    self.callback = dict()
    self.set_handler(ButtonHandler())
    GPIO.setmode(GPIO.BCM)
    for btn in [self.BUTTON1, self.BUTTON2, self.BUTTON3, self.BUTTON4, self.SNOOZE]:
      GPIO.setup(btn,GPIO.IN,pull_up_down=GPIO.PUD_UP)
      GPIO.add_event_detect(btn, GPIO.FALLING, bouncetime=500, callback = self._button_pressed)

  def _button_pressed(self, channel):
    self.callback[channel]()

  def set_handler(self, handler):
    self.callback[self.BUTTON1] = handler.button1_pressed
    self.callback[self.BUTTON2] = handler.button2_pressed
    self.callback[self.BUTTON3] = handler.button3_pressed
    self.callback[self.BUTTON4] = handler.button4_pressed
    self.callback[self.SNOOZE]  = handler.snooze_pressed



class Sound:
  """This class configures and handles the sound."""

  SPEAKER = 16
  
  def __init__(self):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(self.SPEAKER,GPIO.OUT)
    self.music = vlc.MediaPlayer("http://19993.live.streamtheworld.com:80/SKYRADIO_SC")
    self.stop()

  def play(self):
    GPIO.output(self.SPEAKER, GPIO.HIGH)
    self.music.play()
  
  def stop(self):    
    GPIO.output(self.SPEAKER, GPIO.LOW)
    self.music.stop()



class Light:
  """This class configures and handles the light."""

  COLOR_WHITE = neopixel.Color(255,255,255)
  COLOR_BLACK = neopixel.Color(  0,  0,  0)
  
  def __init__(self):
    # LED strip configuration:
    LED_COUNT      = 24      # Number of LED pixels.
    LED_PIN        = 21
    LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
    LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
    LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
    LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
    LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
    LED_STRIP      = neopixel.ws.WS2811_STRIP_GRB   # Strip type and colour ordering
    self.light = neopixel.Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL, LED_STRIP)
    self.light.begin()
    self.set_color(self.COLOR_BLACK)

  def set_color(self, color):
    for i in range(self.light.numPixels()):
      self.light.setPixelColor(i, color)
    self.light.show()
  

backlight = Backlight()
buttons = Buttons()
sound = Sound()
light = Light()
